package ex.money.futures.common;

import tk.mybatis.mapper.common.Mapper;

/**
 * Author: vincent
 * Date: 2018-09-17 14:30:00
 * Comment: 基类 mapper，该类不可以被扫描到，否者会报错
 */

public interface BaseMapper<T> extends Mapper<T> {
}
