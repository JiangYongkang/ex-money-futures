package ex.money.futures;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan(basePackages = "ex.money.futures.mapper")
public class ExMoneyFuturesApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExMoneyFuturesApplication.class, args);
    }
}
